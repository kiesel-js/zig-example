const std = @import("std");

const kiesel = @import("kiesel");
const Agent = kiesel.execution.Agent;
const Realm = kiesel.execution.Realm;
const Script = kiesel.language.Script;

// https://www.destroyallsoftware.com/talks/wat
const source_text =
    \\Array(16).join("wat" - 1) + " Batman!"
;

pub fn main() !void {
    var agent = try Agent.init(kiesel.gc.allocator(), .{});
    defer agent.deinit();

    try Realm.initializeHostDefinedRealm(&agent, .{});
    const realm = agent.currentRealm();

    const script = try Script.parse(source_text, realm, null, .{});
    const result = try script.evaluate();
    std.debug.print("{pretty}\n", .{result});
}
