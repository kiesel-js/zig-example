# Kiesel Zig Example

This repository contains a minimal example of how to embed the Kiesel JS engine
in Zig code.

The basic steps are:

1. Declare dependency in `build.zig.zon`:

   ```zig
   .{
       // ...
       .dependencies = .{
           // ...
           .kiesel = .{
               .url = "https://codeberg.org/kiesel-js/kiesel/archive/<commit hash>.tar.gz",
               .hash = "<package hash>",
           },
       },
   }
   ```

2. Add the module to your executable or library in `build.zig`:

   ```zig
   const kiesel = b.dependency("kiesel", .{
       .target = target,
       .optimize = optimize,
       .@"enable-intl" = false, // Enabled by default, reduces size if you don't need `Intl`
   });

   // ...
   exe.root_module.addImport("kiesel", kiesel.module("kiesel"));
   ```

3. Use it! See [`src/main.zig`](./src/main.zig) for a basic example, run
   `zig build run` to try it out.
